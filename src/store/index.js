import Vue from "vue";
import Vuex from "vuex";
import auth from "./modules/auth";
import user from "./modules/user";
import statistics from "./modules/statistics";
import screening from "./modules/screening";
import signup from "./modules/signup";
import cases from "./modules/cases";

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== "production";

export default new Vuex.Store({
  modules: {
    auth,
    user,
    statistics,
    screening,
    signup,
    cases
  },
  strict: debug
});