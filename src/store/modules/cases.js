import axios from 'axios'

const state = {
    symptoms: [],
    symptomState: false,
    cases: [],
    totalPage: 0,
};
const getters = {
    getSymptomsGetter: state => state.symptoms,
};
const actions = {
    'getSymptoms': ({commit}) => {
        return new Promise((resolve, reject) => {
            axios.get('https://berimcafe.com/data/api_v1/symptom').then((resp) => {
                commit('symptomMutation', resp.data);
                resolve(resp)
            }).catch((err) => {
                reject(err)
            })
        })
    },
    'PostSymptomCase': ({commit},payload) => {
        return new Promise((resolve, reject) => {
            axios.post('https://berimcafe.com/data/api_v1/case_symptom', payload).then((resp) => {
                commit('SymptomStateMutation');
                resolve(resp)
            }).catch((err) => {
                reject(err)
            })
        })
    },
    'PostCase': ({commit},payload) => {
        return new Promise((resolve, reject) => {
            axios({
                method: 'post',
                url: 'https://berimcafe.com/data/api_v1/case',
                data: payload,
                 headers: {
                        'Content-Type': 'multipart/form-data'
                    }
            }).then((resp) => {
                commit('SymptomStateMutation');
                resolve(resp)
            }).catch((err) => {
                reject(err)
            })
        })
    },
    'getAndSetCases':({commit}, page_number)=>{
        return new Promise(((resolve, reject) => {
            axios.get(`https://berimcafe.com/data/api_v1/case?page=${page_number}`).then((resp) =>{
                commit('pageMutation', Math.ceil(resp.data['total']/resp.data['page_size']));
                commit('caseMutation', resp.data.results);
                return resolve(resp.results)
            }).catch((err) => {
                return reject(err);
            })
        }))

    }
};

const mutations = {
    'symptomMutation': (state, payload) => {
        state.symptoms = payload
    },
    'SymptomStateMutation':(state) =>{
        state.symptomState = true
    },
    'pageMutation': (state, payload) =>{
        state.totalPage = payload
    },
    'caseMutation': (state, payload) => {
        state.cases = payload
    }
};

export default {
    state,
    getters,
    actions,
    mutations
}