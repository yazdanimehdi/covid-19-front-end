import axios from 'axios'
import Vue from "vue";

const state = {
    screeningPostStatus: 'false',
    results: []
};

const actions = {
    'getResults': ({commit}) => {
        axios.get('https://berimcafe.com/data/api_v1/get_screening').then((resp) => {
            commit('updateResultsList', resp.data)
        }).catch((err) => {
            console.log(err)
        })
    },

    'postScreeningData': ({commit, dispatch}, payload) => {
        commit('updateScreeningPostStatus', 'loading');
        axios.post('https://berimcafe.com/data/api_v1/post_screening', payload).then(
            () => {
                commit('updateScreeningPostStatus', 'true');
                dispatch('getResults');
            }
        ).catch((err) => {
            console.log(err);
            commit('updateScreeningPostStatus', 'false')
        })

    },

};

const mutations = {
    'updateScreeningPostStatus': (state, payload) => {
        Vue.set(state, 'screeningPostStatus', payload);
    },
    'updateResultsList': (state, payload) => {
        state.results = payload.sort((a, b) => new Date(b.dateTime) - new Date(a.dateTime));
    }

};

export default {
    state,
    actions,
    mutations
};


