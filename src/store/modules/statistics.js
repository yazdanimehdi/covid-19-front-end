import axios from 'axios'
import Vue from "vue";
import https from 'https';

const state = {
    worldStat: {},
    countryStat: [],
    statusStat: 'false',
    statusChart: 'false',
    worldChartDataTotal: [],
    worldChartDataFatal: [],

};

const actions = {
    'getStatisticsData': ({commit}) => {
        commit('updateStatusLoading');
        const agent = new https.Agent({
            rejectUnauthorized: false
        });
        axios.get('https://berimcafe.com/data/api_v1/get_statistics', { httpsAgent: agent }).then(function (resp) {
                commit('updateStatisticsMutation', resp.data)
            }
        ).catch(function (err) {
            console.log(err);
            commit('updateStatusFalse')
        })
    },
    'getWorldChartData': ({commit}) => {
        commit('updateStatusChartLoading');
        axios.get('https://berimcafe.com/data/api_v1/get_world_chart').then(function (resp) {
                commit('updateStatusChartTrue');
                commit('updateChartDataTotalMutation', resp.data['confirmed']);
                commit('updateChartDataFatalMutation', resp.data['fatal'])
            }
        ).catch(function (err) {
            console.log(err);
            commit('updateStatusChartFalse')
        })
    }
};

const mutations = {
    "updateStatisticsMutation": (state, payload) => {
        Vue.set(state, 'worldStat', payload['total']);
        Vue.set(state, 'countryStat', payload['country']);
        Vue.set(state, 'statusStat', 'true');

    },
    "updateStatusLoading": (state) => {
        Vue.set(state, 'statusStat', 'loading');
    },
    "updateStatusFalse": (state) => {
        Vue.set(state, 'statusStat', 'false');
    },
    'updateStatusChartLoading': (state) => {
        Vue.set(state, 'statusChart', 'loading');
    },
    'updateChartDataTotalMutation': (state, payload) => {
        Vue.set(state, 'worldChartDataTotal', payload);
    },
    'updateChartDataFatalMutation': (state, payload) => {
        Vue.set(state, 'worldChartDataFatal', payload);
    },
    'updateStatusChartFalse': (state) => {
        Vue.set(state, 'statusChart', 'false');
    },
    'updateStatusChartTrue': (state) => {
        Vue.set(state, 'statusChart', 'true');
    },
};

export default {
    state,
    actions,
    mutations
};


