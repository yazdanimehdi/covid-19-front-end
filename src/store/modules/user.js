import {USER_REQUEST, USER_ERROR, USER_SUCCESS} from "../actions/user";
import Vue from "vue";
import {AUTH_LOGOUT} from "../actions/auth";
import axios from 'axios'

const state = {status: "", profile: null, locale:"en", user_id: 0};

const getters = {
    getProfile: state => state.profile,
    getId: state => state.user_id,
};

const actions = {
    [USER_REQUEST]: ({commit, dispatch}) => {
        commit(USER_REQUEST);
        axios.get('https://berimcafe.com/data/api_v1/profile')
            .then(resp => {
                commit(USER_SUCCESS, resp.data);

            })
            .catch(() => {
                commit(USER_ERROR);
                // if resp is unauthorized, logout, to
                dispatch(AUTH_LOGOUT);
            });
    },
    'changeLocale':({commit}, payload) =>{
        commit('changeLocaleMutation', payload)
    }
};

const mutations = {
    'changeLocaleMutation': (state, payload)=>{
        state.locale = payload;
    },
    [USER_REQUEST]: state => {
        state.status = "loading";
    },
    [USER_SUCCESS]: (state, resp) => {
        state.status = "success";
        Vue.set(state, "profile", resp['username']);
        Vue.set(state, "user_id", resp['id']);
    },
    [USER_ERROR]: state => {
        state.status = "error";
    },
    [AUTH_LOGOUT]: state => {
        state.profile = null;
    }
};

export default {
    state,
    getters,
    actions,
    mutations
};