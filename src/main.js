import Vue from 'vue'
import App from './App.vue'
import store from './store'
import router from './router'
import vuetify from './plugins/vuetify';
import i18n from './i18n'
import axios from 'axios'
import moment from "moment";

Vue.config.productionTip = false;
router.beforeEach((to, from, next) => {
    let language = to.params.lang;
    if (!language) {
        language = 'en';
    }
    i18n.locale = language;
    next()
})

const token = localStorage.getItem('user-token');

if (token) {
  axios.defaults.headers.common['Authorization'] = `Token ${token}`
}

Vue.filter('formatDate', function (d) {
    return moment.utc(d).format('MMM DD, YYYY  HH:mm')
}) ;


new Vue({
    store,
    router,
    vuetify,
    i18n,
    render: h => h(App)
}).$mount('#app')
