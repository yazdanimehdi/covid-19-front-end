import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home'
import SignupView from "../views/SignupView";
import i18n from "../i18n";
import CasesView from "@/views/CasesView";
import TrackerView from "@/views/TrackerView";
import AddCaseView from "@/views/AddCaseView";
import LoginView from "@/views/LoginView";
import WhatIsCovidView from "@/views/WhatIsCovidView";
import WhatCanYouDoView from "@/views/WhatCanYouDoView";
import TestView from "@/views/TestView";

Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        redirect:`/${i18n.locale}`
    },
    {
        path: '/:lang',
        component:{
            render(c){return c('router-view')}
        },
        children: [
            {
                path: '/',
                name: 'Home',
                component: Home
            },
            {
                path: 'signup',
                name: 'SignupView',
                component: SignupView
            },

             {
                path: 'cases',
                name: 'CasesView',
                component: CasesView
            },

            {
                path: 'tracker',
                name: 'Tracker',
                component: TrackerView
            },
            {
                path: 'login',
                name: 'Login',
                component: LoginView
            },
            {
              path: 'new_case',
              name: 'NewCase',
               component: AddCaseView
            },
            {
                path:'about_covid',
                name:'AboutCovid',
                component: WhatIsCovidView
            },
             {
                path:'what_todo',
                name:'WhatCanYouDo',
                component: WhatCanYouDoView
            },
            {
                path:'test_info',
                name:'TestInformation',
                component: TestView
            }
        ]
    }
]

const router = new VueRouter({
    routes
})

export default router
